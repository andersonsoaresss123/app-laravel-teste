@extends('layouts.layout')

@section('content')

    <form method="POST" action="{{ route('militantes.store') }}">
        @csrf
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="nome">Nome:</label>
                <input type="text" class="form-control" id="nome" name="nome" required>
            </div>

            <div class="form-group col-md-4">
                <label for="sobrenome">Sobrenome:</label>
                <input type="text" class="form-control" id="sobrenome" name="sobrenome" required>
            </div>

            <div class="form-group col-md-4">
                <label for="data_nascimento">Data de Nascimento:</label>
                <input type="date" class="form-control" id="data_nascimento" name="data_nascimento" required>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="genero">Gênero:</label>
                <select class="form-control" id="genero" name="genero" required>
                    <option value="masculino">Masculino</option>
                    <option value="feminino">Feminino</option>
                    <option value="outro">Outro</option>
                </select>
            </div>

            <div class="form-group col-md-4">
                <label for="provincia">Província:</label>
                <select class="form-control" id="provincia" name="provincia" required>
                    <option value="Bengo">Bengo</option>
                    <option value="Benguela">Benguela</option>
                    <option value="Bié">Bié</option>
                    <option value="Cabinda">Cabinda</option>
                    <option value="Cuando Cubango">Cuando Cubango</option>
                    <option value="Cuanza Norte">Cuanza Norte</option>
                    <option value="Cuanza Sul">Cuanza Sul</option>
                    <option value="Cunene">Cunene</option>
                    <option value="Huambo">Huambo</option>
                    <option value="Huíla">Huíla</option>
                    <option value="Luanda">Luanda</option>
                    <option value="Lunda Norte">Lunda Norte</option>
                    <option value="Lunda Sul">Lunda Sul</option>
                    <option value="Malanje">Malanje</option>
                    <option value="Moxico">Moxico</option>
                    <option value="Namibe">Namibe</option>
                    <option value="Uíge">Uíge</option>
                    <option value="Zaire">Zaire</option>
                </select>
            </div>

            <div class="form-group col-md-4">
                <label for="municipio">Município:</label>
                <select class="form-control" id="municipio" name="municipio" required>
                    <option value="Belas">Belas</option>
                    <option value="Cacuaco">Cacuaco</option>
                    <option value="Cazenga">Cazenga</option>
                    <option value="Ícolo e Bengo">Ícolo e Bengo</option>
                    <option value="Luanda">Luanda</option>
                    <option value="Quilamba Quiaxi">Quilamba Quiaxi</option>
                    <option value="Quissama">Quissama</option>
                    <option value="Talatona">Talatona</option>
                    <!-- Adicione os demais municípios de Luanda aqui, se necessário -->
                </select>
            </div>

            <div class="form-group col-md-4">
                <label for="distrito">Distrito:</label>
                <select class="form-control" id="distrito" name="distrito" required>
                    <option value="Distrito A">Distrito A</option>
                    <option value="Distrito B">Distrito B</option>
                    <option value="Distrito C">Distrito C</option>
                    <option value="Distrito D">Distrito D</option>
                    <!-- Adicione os demais distritos de Quilamba Quiaxi aqui, se necessário -->
                </select>
            </div>
            
            

            <div class="form-group col-md-4">
                <label for="endereco">Endereço:</label>
                <input type="text" class="form-control" id="endereco" name="endereco" required>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="email">E-mail:</label>
                <input type="email" class="form-control" id="email" name="email" required>
            </div>

            <div class="form-group col-md-6">
                <label for="telefone">Telefone:</label>
                <input type="tel" class="form-control" id="telefone" name="telefone" required>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="filiacao_partidaria">Filiação Partidária:</label>
                <input type="text" class="form-control" id="filiacao_partidaria" name="filiacao_partidaria" required>
            </div>

            <div class="form-group col-md-6">
                <label for="cargo_no_partido">Cargo no Partido:</label>
                <input type="text" class="form-control" id="cargo_no_partido" name="cargo_no_partido" required>
            </div>
        </div>

        <div class="form-group">
            <label for="data_adesao_partido">Data de Adesão ao Partido:</label>
            <input type="date" class="form-control" id="data_adesao_partido" name="data_adesao_partido" required>
        </div>

        <div class="form-group">
            <label for="status_atividade">Status de Atividade:</label>
            <input type="text" class="form-control" id="status_atividade" name="status_atividade" required>
        </div>

        <button type="submit" class="btn btn-primary">Cadastrar</button>
    </form>



@endsection
