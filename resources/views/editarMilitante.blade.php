@extends('layouts.layout')

@section('content')
    <form action="{{ route('militante.update', $militante->id) }}" method="POST" >
        @csrf()
        @method('PUT')
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="nome">Nome:</label>
                <input type="text" class="form-control" id="nome" name="nome" value="{{ $militante->nome }}" required>
            </div>

            <div class="form-group col-md-4">
                <label for="sobrenome">Sobrenome:</label>
                <input type="text" class="form-control" id="sobrenome" name="sobrenome"
                    value="{{ $militante->sobrenome }}" required>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="genero">Gênero:</label>
                <select class="form-control" id="genero" name="genero" required>
                    <option value="masculino" @if ($militante->genero == 'masculino') selected @endif>Masculino</option>
                    <option value="feminino" @if ($militante->genero == 'feminino') selected @endif>Feminino</option>
                    <option value="outro" @if ($militante->genero == 'outro') selected @endif>Outro</option>
                </select>
            </div>

            <div class="form-group col-md-4">
                <label for="provincia">Província:</label>
                <select class="form-control" id="provincia" name="provincia" required>
                    <option value="Bengo" @if ($militante->provincia == 'Bengo') selected @endif>Bengo</option>
                    <option value="Benguela" @if ($militante->provincia == 'Benguela') selected @endif>Benguela</option>
                    <option value="Bié" @if ($militante->provincia == 'Bié') selected @endif>Bié</option>
                    <option value="Cabinda" @if ($militante->provincia == 'Cabinda') selected @endif>Cabinda</option>
                    <option value="Cuando Cubango" @if ($militante->provincia == 'Cuando Cubango') selected @endif>Cuando Cubango</option>
                    <option value="Cuanza Norte" @if ($militante->provincia == 'Cuanza Norte') selected @endif>Cuanza Norte</option>
                    <option value="Cuanza Sul" @if ($militante->provincia == 'Cuanza Sul') selected @endif>Cuanza Sul</option>
                    <option value="Cunene" @if ($militante->provincia == 'Cunene') selected @endif>Cunene</option>
                    <option value="Huambo" @if ($militante->provincia == 'Huambo') selected @endif>Huambo</option>
                    <option value="Huíla" @if ($militante->provincia == 'Huíla') selected @endif>Huíla</option>
                    <option value="Luanda" @if ($militante->provincia == 'Luanda') selected @endif>Luanda</option>
                    <option value="Lunda Norte" @if ($militante->provincia == 'Lunda Norte') selected @endif>Lunda Norte</option>
                    <option value="Lunda Sul" @if ($militante->provincia == 'Lunda Sul') selected @endif>Lunda Sul</option>
                    <option value="Malanje" @if ($militante->provincia == 'Malanje') selected @endif>Malanje</option>
                    <option value="Moxico" @if ($militante->provincia == 'Moxico') selected @endif>Moxico</option>
                    <option value="Namibe" @if ($militante->provincia == 'Namibe') selected @endif>Namibe</option>
                    <option value="Uíge" @if ($militante->provincia == 'Uíge') selected @endif>Uíge</option>
                    <option value="Zaire" @if ($militante->provincia == 'Zaire') selected @endif>Zaire</option>
                </select>
            </div>

            <div class="form-group col-md-4">
                <label for="municipio">Município:</label>
                <select class="form-control" id="municipio" name="municipio" required>
                    <option value="Belas" @if ($militante->municipio == 'Belas') selected @endif>Belas</option>
                    <option value="Cacuaco" @if ($militante->municipio == 'Cacuaco') selected @endif>Cacuaco</option>
                    <option value="Cazenga" @if ($militante->municipio == 'Cazenga') selected @endif>Cazenga</option>
                    <option value="Ícolo e Bengo" @if ($militante->municipio == 'Ícolo e Bengo') selected @endif>Ícolo e Bengo</option>
                    <option value="Luanda" @if ($militante->municipio == 'Luanda') selected @endif>Luanda</option>
                    <option value="Quilamba Quiaxi" @if ($militante->municipio == 'Quilamba Quiaxi') selected @endif>Quilamba Quiaxi
                    </option>
                    <option value="Quissama" @if ($militante->municipio == 'Quissama') selected @endif>Quissama</option>
                    <option value="Talatona" @if ($militante->municipio == 'Talatona') selected @endif>Talatona</option>
                </select>
            </div>

            <div class="form-group col-md-4">
                <label for="distrito">Distrito:</label>
                <select class="form-control" id="distrito" name="distrito" required>
                    <option value="Distrito A" @if ($militante->distrito == 'Distrito A') selected @endif>Distrito A</option>
                    <option value="Distrito B" @if ($militante->distrito == 'Distrito B') selected @endif>Distrito B</option>
                    <option value="Distrito C" @if ($militante->distrito == 'Distrito C') selected @endif>Distrito C</option>
                    <option value="Distrito D" @if ($militante->distrito == 'Distrito D') selected @endif>Distrito D</option>
                </select>
            </div>

            <div class="form-group col-md-4">
                <label for="endereco">Endereço:</label>
                <input type="text" class="form-control" id="endereco" name="endereco" value="{{ $militante->endereco }}"
                    required>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="email">E-mail:</label>
                <input type="email" class="form-control" id="email" name="email" value="{{ $militante->email }}"
                    required>
            </div>

            <div class="form-group col-md-6">
                <label for="telefone">Telefone:</label>
                <input type="tel" class="form-control" id="telefone" name="telefone" value="{{ $militante->telefone }}"
                    required>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="filiacao_partidaria">Filiação Partidária:</label>
                <input type="text" class="form-control" id="filiacao_partidaria" name="filiacao_partidaria"
                    value="{{ $militante->filiacao_partidaria }}" required>
            </div>

            <div class="form-group col-md-6">
                <label for="cargo_no_partido">Cargo no Partido:</label>
                <input type="text" class="form-control" id="cargo_no_partido" name="cargo_no_partido" value="{{ $militante->cargo_no_partido }}" required>
            </div>
        </div>

        <div class="form-group">
            <label for="data_adesao_partido">Data de Adesão ao Partido:</label>
            <input type="date" class="form-control" id="data_adesao_partido" name="data_adesao_partido" value="{{ $militante->data_adesao_partido }}" required>
        </div>

        <div class="form-group">
            <label for="status_atividade">Status de Atividade:</label>
            <input type="text" class="form-control" id="status_atividade" name="status_atividade" value="{{ $militante->status_atividade }}" required>
        </div>

        <button type="submit" class="btn btn-primary">Editar</button>
    </form>
@endsection
