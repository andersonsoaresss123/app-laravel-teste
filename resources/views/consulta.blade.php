@extends('layouts.layout')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Tables</h1>
        <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
            For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official
                DataTables documentation</a>.</p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>sobrenome</th>
                                <th>data_nascimento</th>
                                <th>genero</th>
                                <th>provincia</th>
                                <th>municipio</th>
                                <th>distrito</th>
                                <th>endereco</th>
                                <th>email</th>
                                <th>telefone</th>
                                <th>filiacao_partidaria</th>
                                <th>cargo_no_partido</th>
                                <th>data_adesao_partido</th>
                                <th>status_atividade</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>sobrenome</th>
                                <th>data_nascimento</th>
                                <th>genero</th>
                                <th>provincia</th>
                                <th>municipio</th>
                                <th>distrito</th>
                                <th>endereco</th>
                                <th>email</th>
                                <th>telefone</th>
                                <th>filiacao_partidaria</th>
                                <th>cargo_no_partido</th>
                                <th>data_adesao_partido</th>
                                <th>status_atividade</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($militantes as $militante)
                                <tr>
                                    <td>{{ $militante->nome }}</td>
                                    <td>{{ $militante->sobrenome }}</td>
                                    <td>{{ $militante->data_nascimento }}</td>
                                    <td>{{ $militante->genero }}</td>
                                    <td>{{ $militante->provincia }}</td>
                                    <td>{{ $militante->municipio }}</td>
                                    <td>{{ $militante->distrito }}</td>
                                    <td>{{ $militante->endereco }}</td>
                                    <td>{{ $militante->email }}</td>
                                    <td>{{ $militante->telefone }}</td>
                                    <td>{{ $militante->filiacao_partidaria }}</td>
                                    <td>{{ $militante->cargo_no_partido }}</td>
                                    <td>{{ $militante->data_adesao_partido }}</td>
                                    <td>{{ $militante->status_atividade }}</td>
                                    <td>
                                        <form action="{{ route('militante.eliminar', $militante->id) }}" method="POST" onsubmit="return confirmDelete();">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger"><i
                                                    class="bi bi-trash-fill"></i></button>
                                        </form>
                                    </td>
                                    <td>
                                        <a class="btn btn-primary"
                                            href="{{ route('militante.editar', $militante->id) }}"><i
                                                class="bi bi-pencil-square"></i></a>
                                    </td>
                                    <td>
                                        <a class="btn btn-primary" href="{{ route('militante.show', $militante->id) }}"><i
                                                class="bi bi-eye"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <script>
        function confirmDelete() {
            return confirm('Tem certeza de que deseja eliminar este militante? Esta ação não pode ser desfeita.');
        }
    </script>
    
@endsection
