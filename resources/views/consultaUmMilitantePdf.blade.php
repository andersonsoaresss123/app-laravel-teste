<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detalhes do Militante</title>
    <!-- Bootstrap CSS incorporado -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        /* Ajuste de margens e tamanhos para caber na folha A4 */
        @page {
            size: A4;
            margin: 20mm;
        }
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            font-size: 12px;
            line-height: 1.5;
        }
        .container {
            max-width: 100%;
            padding: 10px;
        }
        .row {
            margin-bottom: 10px;
        }
        .card {
            margin: auto;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .card-img-top {
            max-width: 200px;
            height: auto;
            object-fit: cover;
            border-radius: 50%;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .card-title {
            font-size: 24px;
            margin-bottom: 10px;
            color: #333;
        }
        .list-group-item {
            padding: 10px 15px;
        }
        .badge {
            font-size: 100%;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="text-center">
                    <img src="data:image/png;base64,{{ base64_encode(file_get_contents(public_path('storage/66a7d284eeddc.png'))) }}" alt="Foto de {{ $militante->nome }}" class="card-img-top">
                </div>
                <div class="card-body">
                    <h1 class="card-title text-center">{{ $militante->nome }} {{ $militante->sobrenome }}</h1>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><strong>Data de Nascimento:</strong> {{ $militante->data_nascimento }}</li>
                        <li class="list-group-item"><strong>Gênero:</strong> <span class="badge badge-info">{{ ucfirst($militante->genero) }}</span></li>
                        <li class="list-group-item"><strong>Província:</strong> {{ $militante->provincia }}</li>
                        <li class="list-group-item"><strong>Município:</strong> {{ $militante->municipio }}</li>
                        <li class="list-group-item"><strong>Distrito:</strong> {{ $militante->distrito }}</li>
                        <li class="list-group-item"><strong>Endereço:</strong> {{ $militante->endereco }}</li>
                        <li class="list-group-item"><strong>E-mail:</strong> {{ $militante->email }}</li>
                        <li class="list-group-item"><strong>Telefone:</strong> {{ $militante->telefone }}</li>
                        <li class="list-group-item"><strong>Filiação Partidária:</strong> {{ $militante->filiacao_partidaria }}</li>
                        <li class="list-group-item"><strong>Cargo no Partido:</strong> {{ $militante->cargo_no_partido }}</li>
                        <li class="list-group-item"><strong>Data de Adesão ao Partido:</strong> {{ $militante->data_adesao_partido }}</li>
                        <li class="list-group-item"><strong>Status de Atividade:</strong> <span class="badge badge-success">{{ $militante->status_atividade }}</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
