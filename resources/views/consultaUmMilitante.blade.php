@extends('layouts.layout')

@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-4 text-center">
                <!-- Exibindo a foto do militante com borda arredondada e sombra -->
                <img src="{{ asset('storage/66a7d284eeddc.png') }}" alt="Foto de {{ $militante->nome }}"
                    class="img-fluid rounded shadow" style="width: 100%; height: auto; max-width: 250px; object-fit: cover;">
            </div>
            <div class="col-md-8">
                <h1 class="display-4">{{ $militante->nome }} {{ $militante->sobrenome }}</h1>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><strong>Data de Nascimento:</strong> {{ $militante->data_nascimento }}</li>
                    <li class="list-group-item"><strong>Gênero:</strong> {{ ucfirst($militante->genero) }}</li>
                    <li class="list-group-item"><strong>Província:</strong> {{ $militante->provincia }}</li>
                    <li class="list-group-item"><strong>Município:</strong> {{ $militante->municipio }}</li>
                    <li class="list-group-item"><strong>Distrito:</strong> {{ $militante->distrito }}</li>
                    <li class="list-group-item"><strong>Endereço:</strong> {{ $militante->endereco }}</li>
                    <li class="list-group-item"><strong>E-mail:</strong> {{ $militante->email }}</li>
                    <li class="list-group-item"><strong>Telefone:</strong> {{ $militante->telefone }}</li>
                    <li class="list-group-item"><strong>Filiação Partidária:</strong> {{ $militante->filiacao_partidaria }}
                    </li>
                    <li class="list-group-item"><strong>Cargo no Partido:</strong> {{ $militante->cargo_no_partido }}</li>
                    <li class="list-group-item"><strong>Data de Adesão ao Partido:</strong>
                        {{ $militante->data_adesao_partido }}</li>
                    <li class="list-group-item"><strong>Status de Atividade:</strong> {{ $militante->status_atividade }}
                    </li>
                </ul><br>
                <a class="btn btn-primary" href="{{ route('pdf', $militante->id) }}"> Imprimir <i
                        class="bi bi-printer"></i></a>
            </div>
        </div>
    </div>
@endsection
