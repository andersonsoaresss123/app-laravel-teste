<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Capturar Foto</title>
    <style>
        .container {
            display: flex;
            justify-content: flex-start;
            align-items: center;
        }
        .preview-container {
            margin-left: 20px;
        }
        #photoPreview {
            max-width: 100%;
            height: auto;
        }
    </style>
</head>
<body>
    <h1>Capturar Foto da Webcam</h1>
    <div class="container">
        <div>
            <video id="webcam" autoplay></video>
            <canvas id="canvas" style="display: none;"></canvas>
            <button id="captureButton">Capturar Foto</button>
        </div>
        <div id="previewContainer" class="preview-container" style="display: none;">
            <h2>Pré-visualização</h2>
            <img id="photoPreview" src="" alt="Pré-visualização da foto capturada">
            <button id="saveButton">Salvar Foto</button>
        </div>
    </div>

    <form action="{{ route('webcam.salvarFoto') }}" method="POST" id="formSalvarFoto" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="photo" id="photoInput">
    </form>

    <script>
        const video = document.getElementById('webcam');
        const canvas = document.getElementById('canvas');
        const captureButton = document.getElementById('captureButton');
        const photoInput = document.getElementById('photoInput');
        const formSalvarFoto = document.getElementById('formSalvarFoto');
        const previewContainer = document.getElementById('previewContainer');
        const photoPreview = document.getElementById('photoPreview');
        const saveButton = document.getElementById('saveButton');

        // Acessa a webcam
        navigator.mediaDevices.getUserMedia({ video: true })
            .then(stream => {
                video.srcObject = stream;
            })
            .catch(err => console.error('Erro ao acessar a webcam: ', err));

        // Captura a foto e a coloca no input hidden e na pré-visualização
        captureButton.addEventListener('click', () => {
            const context = canvas.getContext('2d');
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            context.drawImage(video, 0, 0, canvas.width, canvas.height);
            const dataURL = canvas.toDataURL('image/png');
            photoInput.value = dataURL;
            photoPreview.src = dataURL; // Atualiza a pré-visualização da imagem
            previewContainer.style.display = 'block'; // Exibe a pré-visualização
        });

        // Submete o formulário ao clicar no botão de salvar
        saveButton.addEventListener('click', () => {
            formSalvarFoto.submit();
        });
    </script>
</body>
</html>
