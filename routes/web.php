<?php

use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\MilitanteController;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\WebcamController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('login');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/webcam', [WebcamController::class, 'index'])->name('webcam.index');
Route::post('/salvar-foto', [WebcamController::class, 'salvarFoto'])->name('webcam.salvarFoto');

// Route::get('/umMilitante/{id}', function () {
//     return view('consultaUmMilitante');
// });

Route::put('/updateMilitante/{id}', [MilitanteController::class, 'update'])->name('militante.update');
Route::get('/umMilitante/{id}', [MilitanteController::class, 'show'])->name('militante.show');
Route::get('/editarMilitante/{id}', [MilitanteController::class, 'editar'])->name('militante.editar');
Route::delete('/eliminarMilitante/{id}', [MilitanteController::class, 'eliminar'])->name('militante.eliminar');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::get('/consulta', [MilitanteController::class, 'listarMilitantes'])->name('militantes.listar');
    Route::post('/cadastrar', [MilitanteController::class, 'store'])->name('militantes.store');
    Route::get('/cadastrar', function(){
        return view('cadastrar');
    })->name('militantes.cadastrar');
    Route::get('/gerar-pdf/{id}', [PdfController::class, 'generatePdf'])->name('pdf');
    Route::get('/registar', function(){
        return view('register');
    });
    Route::post('/registar', [RegisteredUserController::class, 'store'])->name('register.store');
});

require __DIR__.'/auth.php';
