<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Militante extends Model
{
    // protected $table = 'militante';
    protected $fillable = [
        'nome',
        'sobrenome',
        'data_nascimento',
        'genero',
        'provincia',
        'municipio',
        'distrito',
        'endereco',
        'email',
        'telefone',
        'filiacao_partidaria',
        'cargo_no_partido',
        'data_adesao_partido',
        'status_atividade',
        'foto',
    ];
  
}

