<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Militante;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class MilitanteController extends Controller
{
    public function store(Request $request)
    {
        // Validação dos dados do formulário
        $validatedData = $request->validate([
            'nome' => 'required',
            'sobrenome' => 'required',
            'data_nascimento' => 'required|date',
            'genero' => 'required',
            'provincia' => 'required',
            'municipio' => 'required',
            'distrito' => 'required',
            'endereco' => 'required',
            'email' => 'required|email|unique:militantes',
            'telefone' => 'required',
            'filiacao_partidaria' => 'required',
            'cargo_no_partido' => 'required',
            'data_adesao_partido' => 'required|date',
            'status_atividade' => 'required',
        ]);

        // Salvar os dados no banco de dados
        Militante::create($validatedData);

        // Redirecionar o usuário para uma página de confirmação ou outra rota
        return redirect()->route('militantes.cadastrar')->with('success', 'Militante cadastrado com sucesso!');
    }

    public function show($id)
    {
        $militante = Militante::findOrFail($id); // Recupera o militante pelo ID
        return view('consultaUmMilitante', compact('militante')); // Retorna a view com os dados do militante
    }

    public function eliminar($id)
    {

        $militante = DB::table('militantes')->where('id', $id)->first();

        if ($militante) {
            DB::table('militantes')->where('id', $id)->delete();
            return Redirect::route('militantes.listar')->with('success', 'Militante deletado com sucesso!');
        } else {
            return Redirect::route('militantes.listar')->with('error', 'Militante não encontrado!');
        }
    }

    public function editar($id)
    {
        $militante = Militante::find($id);
        if ($militante) {
            return view('editarMilitante', compact('militante'));
        } else {
            redirect()->back();
        }
    }
    
    public function update($id, Request $request)
    {
        $militante = Militante::find($id);
        // dd($militante);
      
        if ($militante) {
            $militante->update($request->only([
                'nome',
                'sobrenome',
                'data_nascimento',
                'genero',
                'provincia',
                'municipio',
                'distrito',
                'endereco',
                'email',
                'telefone',
                'filiacao_partidaria',
                'cargo_no_partido',
                'data_adesao_partido',
                'status_atividade',
            ]));
           
            return Redirect::route('militantes.listar')->with('success', 'Militante actualizado com sucesso!');
        } else {
            redirect()->back();
        }
    }

    public function listarMilitantes()
    {
        $militantes = Militante::all();
        return view('consulta', ['militantes' => $militantes]);
    }
}
