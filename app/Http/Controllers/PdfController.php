<?php

namespace App\Http\Controllers;

use App\Models\Militante;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Http\Request;

class PdfController extends Controller
{
    public function generatePdf($id)
    {
        $militante = Militante::findOrFail($id);

        // Crie uma instância do Dompdf
        $dompdf = new Dompdf();

        // Renderize a visualização HTML para PDF
        $html = view('consultaUmMilitantePdf', compact('militante'))->render();

        // Adicione o estilo CSS ao HTML
        // $html = '<style>' . $css . '</style>' . $html;

        $dompdf->loadHtml($html);

        // Defina as opções de renderização (opcional)
        $options = new Options();
        $options->set(['isHtml5ParserEnabled'=>true, 'isRemoteEnabled'=>true]);

        // Aplicar opções
        $dompdf->setOptions($options);

        // Renderize o PDF
        $dompdf->render();

        // Obtenha o conteúdo do PDF
        $pdfContent = $dompdf->output();

        // Retorne o PDF como uma resposta HTTP com o cabeçalho Content-Type adequado
        return response($pdfContent, 200)
            ->header('Content-Type', 'application/pdf');
    }
}
