<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class WebcamController extends Controller
{
    public function index()
    {
        return view('webcam');
    }

    public function salvarFoto(Request $request)
    {
        // Verifica se a foto foi enviada
        if ($request->has('photo')) {
            $photo = $request->input('photo'); // Base64 encoded string

            // Decodifica a string base64
            $photo = str_replace('data:image/png;base64,', '', $photo);
            $photo = str_replace(' ', '+', $photo);
            $photoData = base64_decode($photo);

            // Cria um nome de arquivo único
            $fileName = uniqid() . '.png';

            // Salva a foto no storage
            Storage::disk('public')->put($fileName, $photoData);

            // Retorna uma resposta ou redireciona para outra página
            return redirect()->back()->with('success', 'Foto salva com sucesso!');
        }

        return redirect()->back()->with('error', 'Erro ao salvar a foto.');
    }
}
