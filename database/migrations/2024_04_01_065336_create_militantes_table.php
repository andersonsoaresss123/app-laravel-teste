<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('militantes', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('sobrenome');
            $table->date('data_nascimento');
            $table->string('genero');
            $table->string('provincia');
            $table->string('municipio');
            $table->string('distrito');
            $table->string('endereco');
            $table->string('email')->unique();
            $table->string('telefone');
            $table->string('filiacao_partidaria');
            $table->string('cargo_no_partido');
            $table->date('data_adesao_partido');
            $table->string('status_atividade');
            $table->string('foto')->nullable();
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('militantes');
    }
};
